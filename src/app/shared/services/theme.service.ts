import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private readonly currentTheme: BehaviorSubject<AppTheme>;

  constructor() {
    this.currentTheme = new BehaviorSubject<AppTheme>('dark');
  }

  public get current(): AppTheme {
    return this.currentTheme.getValue();
  }

  public set current(theme: AppTheme) {
    this.currentTheme.next(theme);
  }

  public asyncCurrent(): BehaviorSubject<AppTheme> {
    return this.currentTheme;
  }
}

export type AppTheme = 'light' | 'dark';

import { Component } from '@angular/core';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent {

  public today = new Date();

  public mainKnowledge: string[];
  public goodKnowledge: string[];
  public otherKnowledge: string[];

  constructor() {
    this.mainKnowledge = [
      'HTML',
      'JavaScript',
      'CSS',
      'RxJS',
      'TypeScript',
      'Angular'
    ];
    this.goodKnowledge = [
      'NodeJS',
      'C++',
      'Java',
      'socket.io'
    ];
    this.otherKnowledge = [
      'ReactJS',
      'Php',
      'Laravel',
      'Python',
      'Arduino',
      'Perl',
      'discord.js'
    ];
  }
}
